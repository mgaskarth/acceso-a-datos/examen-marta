<?php

/** @var yii\web\View $this */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

$this->title = 'My Awesome Exam';
?>
<div class="site-index">

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'dorsal',
        'nombre',
        [
            'class' => ActionColumn::className(),
            'urlCreator' => function ($action, Site $model, $key, $index, $column) {
                return Url::toRoute([$action, 'dorsal' => $model->dorsal]);
            }
        ],
    ],
]);
?>
    
    
</div>



